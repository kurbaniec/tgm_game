# TGM_Game
Das Spiel ist ein Jump 'n' Run Spiel im Stil von Super Mario, was in einem Browser lauffähig ist.

##Hintergrund
Das Programm war eine Aufgabenstellung im zweiten Jahrgang. Ziel war es ein einfaches Spiel in JavaScript und HTML zu erstellen, um uns mit JavaScript 
vertraut zu machen. Es sollten keine Framweworks bei der Aufgabe benutzt werden.

##Start
Einfach das gesamte Repo clonen und *Trouble_In_Rainbowland_1.0.html* öffnen. 

##Hinweis
Die Grafiken und Texturen sind geistiges Eigentum von Christoph Brein.
